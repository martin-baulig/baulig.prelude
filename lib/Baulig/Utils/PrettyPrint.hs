module Baulig.Utils.PrettyPrint
    ( ColorConfig
    , HasColorConfig(..)
    , HasPrettyPrint(..)
    , defaultColorConfig
    , disableColors
      -- New API
    , PrettyPrint
    , withPrettyPrint
    , withPrettyPrint_
    , withPrettyPrintR
    , prettyPrintBS
    , printInterspersed
    , printTitle
    , printHeader
    , printTimestamp
    , printLabel
    , printLabelValue
    , printText
    ) where

import           Baulig.Prelude.Import

import           Control.Monad.Writer

import           Rainbow

import           System.Log.FastLogger

import           Baulig.Utils.Logging  as Import

------------------------------------------------------------------------------
--- LogLevel
------------------------------------------------------------------------------

logLevelColors :: LogLevel -> Chunk -> Chunk
logLevelColors level = case level of
    LevelDebug -> fore grey
    LevelInfo -> fore yellow
    LevelWarn -> fore blue
    LevelError -> fore red
    LevelOther "verbose" -> fore yellow
    LevelOther "normal" -> fore brightGreen
    LevelOther "message" -> fore cyan
    LevelOther "important" -> fore brightRed
    LevelOther _ -> fore magenta

logLevelName :: LogLevel -> Text
logLevelName level = case level of
    LevelDebug -> "debug"
    LevelInfo -> "info"
    LevelWarn -> "warn"
    LevelError -> "error"
    LevelOther "verbose" -> "verbose"
    LevelOther "normal" -> "normal"
    LevelOther "message" -> "message"
    LevelOther "important" -> "important"
    LevelOther other -> other

------------------------------------------------------------------------------
--- ColorConfig
------------------------------------------------------------------------------

data ColorConfig =
    ColorConfig { ccUseColors  :: !Bool
                , ccTitle      :: !(Chunk -> Chunk)
                , ccHeader     :: !(Chunk -> Chunk)
                , ccTimestamp  :: !(Chunk -> Chunk)
                , ccLabel      :: !(Chunk -> Chunk)
                , ccLabelValue :: !(Chunk -> Chunk)
                , ccText       :: !(Chunk -> Chunk)
                , ccLogSource  :: !(Chunk -> Chunk)
                , ccLocation   :: !(Chunk -> Chunk)
                , ccLevelColor :: !(LogLevel -> Chunk -> Chunk)
                }

defaultColorConfig :: ColorConfig
defaultColorConfig =
    ColorConfig { ccUseColors  = True
                , ccTitle      = fore blue
                , ccHeader     = fore yellow
                , ccTimestamp  = fore brightBlue
                , ccLabel      = fore red
                , ccLabelValue = fore brightRed
                , ccText       = fore white
                , ccLogSource  = fore blue
                , ccLocation   = fore grey
                , ccLevelColor = logLevelColors
                }

instance Default ColorConfig where
    def = defaultColorConfig

disableColors :: ColorConfig -> ColorConfig
disableColors cc = cc { ccUseColors = False }

------------------------------------------------------------------------------
--- HasColorConfig
------------------------------------------------------------------------------

class HasColorConfig x where
    colorConfigL :: Lens' x ColorConfig

instance HasColorConfig ColorConfig where
    colorConfigL = id

------------------------------------------------------------------------------
--- HasPrettyPrint
------------------------------------------------------------------------------

class HasPrettyPrint x where
    prettyPrint :: ( MonadIO m
                   , MonadReader env m
                   , HasLogOptions env
                   , HasColorConfig env
                   )
                => x
                -> m ()
    prettyPrint value = do
        convert <- bool toByteStringsColors0 toByteStringsColors256
            <$> view logOptionsUseColor
        chunks <- chunksToByteStrings convert
            <$> withPrettyPrintR (prettyFormat value)
        liftIO $ mapM_ putStr $ chunks <&> decodeUtf8

    prettyFormat :: x -> PrettyPrint ()

prettyPrintBS
    :: HasPrettyPrint x => ColorConfig -> LogOptions -> x -> ByteString
prettyPrintBS colors options func =
    let chunks = withPrettyPrint_ colors $ prettyFormat func
    in concat $ chunksToByteStrings convert chunks
  where
    convert = bool toByteStringsColors0 toByteStringsColors256
        $ options ^. logOptionsUseColor

------------------------------------------------------------------------------
--- PrettyPrinter
------------------------------------------------------------------------------

newtype PrettyPrintM m a =
    PrettyPrintM { runPrettyPrintM :: ColorConfig -> m (a, [Chunk]) }

type PrettyPrint a = PrettyPrintM Identity a

instance Functor m => Functor (PrettyPrintM m) where
    fmap f (PrettyPrintM g) = PrettyPrintM $ \config ->
        fmap (first f) (g config)

instance Monad m => Applicative (PrettyPrintM m) where
    {-# INLINE pure #-}
    pure a = PrettyPrintM $ \_ -> return (a, mempty)

    {-# INLINE (<*>) #-}
    PrettyPrintM mf <*> PrettyPrintM mx = PrettyPrintM $ \r -> do
        ~(f, w) <- mf r
        ~(x, w') <- mx r
        return (f x, w `mappend` w')

instance Monad m => Monad (PrettyPrintM m) where
    m >>= k = PrettyPrintM $ \r -> do
        ~(a, w) <- runPrettyPrintM m r
        ~(b, w') <- runPrettyPrintM (k a) r

        return (b, w `mappend` w')

instance Monad m => MonadReader ColorConfig (PrettyPrintM m) where
    ask = PrettyPrintM $ \r -> return (r, mempty)

    local f m = PrettyPrintM $ \r -> runPrettyPrintM m (f r)

instance Monad m => MonadWriter [Chunk] (PrettyPrintM m) where
    tell w = PrettyPrintM $ \_ -> return ((), w)

    listen m = PrettyPrintM $ \r -> do
        ~(a, w) <- runPrettyPrintM m r
        return ((a, w), w)

    pass m = PrettyPrintM $ \r -> do
        ~((a, f), w) <- runPrettyPrintM m r
        return (a, f w)

withPrettyPrint :: ColorConfig -> PrettyPrint x -> (x, [Chunk])
withPrettyPrint config func = runIdentity $ runPrettyPrintM func config

withPrettyPrint_ :: ColorConfig -> PrettyPrint () -> [Chunk]
withPrettyPrint_ config func =
    snd <$> runIdentity $ runPrettyPrintM func config

withPrettyPrintR
    :: (MonadReader env m, HasColorConfig env) => PrettyPrint () -> m [Chunk]
withPrettyPrintR func = flip withPrettyPrint_ func <$> view colorConfigL

printInterspersed :: HasPrettyPrint x => Text -> [x] -> PrettyPrint ()
printInterspersed _ [] = pure ()
printInterspersed _ [ item ] = prettyFormat item
printInterspersed sep (x : xs) =
    prettyFormat x >> printText sep >> printInterspersed sep xs

printWithColor :: (ColorConfig -> Chunk -> Chunk) -> Text -> PrettyPrint ()
printWithColor getter text = do
    useColor <- asks ccUseColors
    color <- asks getter
    if useColor
        then tell $ singleton $ color $ chunk text
        else tell $ singleton $ chunk text

printTitle :: Text -> PrettyPrint ()
printTitle = printWithColor ccTitle

printHeader :: Text -> PrettyPrint ()
printHeader = printWithColor ccHeader

printTimestamp :: Text -> PrettyPrint ()
printTimestamp = printWithColor ccTimestamp

printLabel :: Text -> PrettyPrint ()
printLabel = printWithColor ccLabel

printLabelValue :: Text -> PrettyPrint ()
printLabelValue = printWithColor ccLabelValue

printText :: Text -> PrettyPrint ()
printText = printWithColor ccText

instance HasPrettyPrint (LogLevel, Maybe LogSource, Text) where
    prettyFormat (level, maybeSource, text) = do
        printWithColor color $ "[" <> logLevelName level <> "]"
        forM_ maybeSource
            $ \source -> printWithColor ccLogSource $ " (" <> source <> ")"
        printWithColor ccText $ " " <> text <> "\n"
      where
        color cc = ccLevelColor cc level

instance HasPrettyPrint ( Maybe FormattedTime
                        , Loc
                        , LogLevel
                        , Maybe LogSource
                        , LogStr
                        ) where
    prettyFormat (maybeTime, loc, level, maybeSource, message) = do
        forM_ maybeTime $ \time -> printWithColor ccTimestamp [i|[#{time}]:|]
        printWithColor color [i|[#{logLevelName level}]|]
        forM_ maybeSource
            $ \source -> printWithColor ccLogSource [i|(#{source})|]
        printWithColor ccText $ " " <> decodeUtf8 (fromLogStr message) <> "\n"
        unless (isDefaultLoc loc) $ do
            printWithColor ccLocation [i|@(#{fileLocStr})\n|]
      where
        color cc = ccLevelColor cc level

        -- taken from file-location package
        -- turn the TH Loc location information into a human readable string
        -- leaving out the loc_end parameter
        fileLocStr = loc_package loc ++ ':' : loc_module loc
            ++ ' ' : loc_filename loc ++ ':' : line loc ++ ':' : char loc
          where
            line = show . fst . loc_start

            char = show . snd . loc_start

isDefaultLoc :: Loc -> Bool
isDefaultLoc (Loc "<unknown>" "<unknown>" "<unknown>" (0, 0) (0, 0)) = True
isDefaultLoc _ = False

