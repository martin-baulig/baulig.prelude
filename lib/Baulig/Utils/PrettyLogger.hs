module Baulig.Utils.PrettyLogger
    ( PrettyLogger
    , HasPrettyLogger(..)
    , makePrettyLogger
    , flushPrettyLogger
    , prettyMessageLogger
    ) where

import           Baulig.Prelude.Import

import           Network.Wai.Logger

import           System.Log.FastLogger

import           Baulig.Utils.Logging
import           Baulig.Utils.PrettyPrint

------------------------------------------------------------------------------
--- PrettyLogger
------------------------------------------------------------------------------

data PrettyLogger = PrettyLogger { plColorConfig :: !ColorConfig
                                 , plOptions     :: !LogOptions
                                 , plLogger      :: !LoggerSet
                                 , plDate        :: !DateCacheGetter
                                 }

instance HasLogOptions PrettyLogger where
    logOptionsL = lens plOptions (\x y -> x { plOptions = y })

makePrettyLogger :: ColorConfig -> LogOptions -> LoggerSet -> IO PrettyLogger
makePrettyLogger color options logger =
    PrettyLogger color options logger . fst <$> clockDateCacher

flushPrettyLogger :: PrettyLogger -> IO ()
flushPrettyLogger = flushLogStr . plLogger

prettyMessageLogger
    :: Maybe (LogSource -> LogLevel -> IO Bool)
    -> PrettyLogger
    -> Loc
    -> LogSource
    -> LogLevel
    -> LogStr
    -> IO ()
prettyMessageLogger maybeCheckLoggable logger loc source level msg =
    whenM checkLoggable $ do
        now <- plDate logger
        pushLogStr (plLogger logger) $ toLogStr $ fromStrict $ format now
        flushLogStr (plLogger logger)
  where
    checkLoggable = case maybeCheckLoggable of
        Just checkFunc -> checkFunc source level
        Nothing -> pure $ logLevelFilter (plOptions logger ^. logOptionsFilter)
                                         source
                                         level

    maybeSource = bool (Just source) Nothing $ null source

    format now = prettyPrintBS (plColorConfig logger)
                               (plOptions logger)
                               (Just now, loc, level, maybeSource, msg)

------------------------------------------------------------------------------
--- HasPrettyLogger
------------------------------------------------------------------------------

class HasPrettyLogger x where
    prettyLoggerL :: Lens' x PrettyLogger

instance HasPrettyLogger PrettyLogger where
    prettyLoggerL = id
