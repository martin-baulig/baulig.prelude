module Baulig.Utils.ConfigFile
    ( hasConfigFile
    , readConfigFile
    , readConfigFileIfExists
    ) where

import           Baulig.Prelude

import qualified Control.Exception              as Exception
import           Control.Monad.Extra

import           Data.Aeson
import           Data.Yaml                      (decodeFileEither)

import           System.Directory
import           System.Environment.XDG.BaseDir

------------------------------------------------------------------------------
--- readConfigFile
------------------------------------------------------------------------------

configSettingsYmlValue :: FilePath -> IO Value
configSettingsYmlValue path =
    either Exception.throw id <$> decodeFileEither path

hasConfigFile :: FilePath -> IO Bool
hasConfigFile path = doesFileExist =<< getUserConfigFile "yesod" path

readConfigFile :: (HasCallStack, FromJSON x) => FilePath -> IO x
readConfigFile file = do
    path <- getUserConfigFile "yesod" file

    value <- configSettingsYmlValue path

    case fromJSON value of
        Error e -> error e
        Success settings -> pure settings

readConfigFileIfExists
    :: (HasCallStack, FromJSON x) => FilePath -> IO (Maybe x)
readConfigFileIfExists path = whenMaybeM (hasConfigFile path)
    $ readConfigFile path
