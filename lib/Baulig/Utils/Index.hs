module Baulig.Utils.Index (RawKey(..), RawIndex(..)) where

import           Baulig.Prelude.Import

import           Database.Persist

------------------------------------------------------------------------------
--- RawKey
------------------------------------------------------------------------------

class RawKey x where
    rawKey :: Key x -> Text

------------------------------------------------------------------------------
--- RawIndex
------------------------------------------------------------------------------

class RawIndex x where
    rawIndex :: x -> Text
