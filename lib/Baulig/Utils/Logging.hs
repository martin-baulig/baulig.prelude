{-# OPTIONS_GHC -Wno-missing-local-signatures #-}

module Baulig.Utils.Logging
    ( CanReplayLog(..)
    , Display(..)
    , LogLevelFilter(..)
    , logLevelFilter
    , LogOptions
    , HasLogOptions(..)
    , LogFunc
    , defaultLogOptions
    , logNormal
    , logNormalS
    , logNormalN
    , logNormalNS
    , logVerbose
    , logVerboseS
    , logVerboseN
    , logVerboseNS
    , logMessage
    , logMessageS
    , logMessageN
    , logMessageNS
    , logImportant
    , logImportantS
    , logImportantN
    , logImportantNS
    , printException
    , intercalateDisplay
    , intercalateDisplay'
    , module Control.Monad.Logger
    ) where

import           Baulig.Prelude.Import      hiding (lift)

import           Control.Monad.Logger

import           GHC.Stack

import           Language.Haskell.TH.Syntax (Exp, Lift(lift), Q, qLocation)

------------------------------------------------------------------------------
--- Display
------------------------------------------------------------------------------

class Display x where
    display :: x -> Text

    default display :: Show x => x -> Text
    display = tshow

instance Display Char

instance Display Int

instance Display Word32

instance Display Word64

instance Display SomeException

instance Display Text

------------------------------------------------------------------------------
--- Log Options
------------------------------------------------------------------------------

type LogFunc = Loc -> LogSource -> LogLevel -> LogStr -> IO ()

data LogLevelFilter = FilterDebug
                    | FilterInfo
                    | FilterNormal
                    | FilterWarn
                    | FilterError
                    | FilterNoLogging
    deriving stock (Enum, Eq, Ord, Show)

logLevelFilter :: LogLevelFilter -> LogSource -> LogLevel -> Bool
logLevelFilter kind _ level
    | kind == FilterNoLogging = False
    | otherwise = case level of
        LevelDebug -> kind == FilterDebug
        LevelInfo -> kind == FilterDebug || kind == FilterInfo
        LevelOther "important" -> kind /= FilterError
        LevelWarn -> kind /= FilterError
        LevelError -> True
        _ -> kind /= FilterWarn && kind /= FilterError

data LogOptions = LogOptions { _logOptionsUseColor :: !Bool
                             , _logOptionsFilter   :: !LogLevelFilter
                             }
    deriving stock (Eq, Show)

class HasLogOptions x where
    logOptionsL :: Lens' x LogOptions

    logOptionsUseColor :: Lens' x Bool
    logOptionsUseColor = logOptionsL . go
      where
        go f x = (\y -> x { _logOptionsUseColor = y })
            <$> f (_logOptionsUseColor x)

    logOptionsFilter :: Lens' x LogLevelFilter
    logOptionsFilter = logOptionsL . go
      where
        go f x = (\y -> x { _logOptionsFilter = y }) <$> f (_logOptionsFilter x)

instance HasLogOptions LogOptions where
    logOptionsL = id

defaultLogOptions :: MonadIO m => m LogOptions
defaultLogOptions = ctor <$> hIsTerminalDevice stdout
  where
    ctor useColor = LogOptions useColor FilterDebug

------------------------------------------------------------------------------
--- Custom Logging
------------------------------------------------------------------------------

logTH :: LogLevel -> Q Exp
logTH level = [|monadLoggerLog $(qLocation >>= liftLoc) (pack "") $(lift level)
              . (id :: Text -> Text)|]

levelNormal :: LogLevel
levelNormal = LevelOther "normal"

levelVerbose :: LogLevel
levelVerbose = LevelOther "verbose"

levelMessage :: LogLevel
levelMessage = LevelOther "message"

levelImportant :: LogLevel
levelImportant = LevelOther "important"

logNormal :: Q Exp
logNormal = logTH levelNormal

logNormalS :: Q Exp
logNormalS = [|\a b ->
             monadLoggerLog $(qLocation >>= liftLoc) a levelNormal (b :: Text)|]

logNormalN :: MonadLogger m => Text -> m ()
logNormalN = logWithoutLoc "" levelNormal

logNormalNS :: MonadLogger m => LogSource -> Text -> m ()
logNormalNS source = logWithoutLoc source levelNormal

logVerbose :: Q Exp
logVerbose = logTH levelVerbose

logVerboseS :: Q Exp
logVerboseS =
    [|\a b ->
    monadLoggerLog $(qLocation >>= liftLoc) a levelVerbose (b :: Text)|]

logVerboseN :: MonadLogger m => Text -> m ()
logVerboseN = logWithoutLoc "" levelVerbose

logVerboseNS :: MonadLogger m => LogSource -> Text -> m ()
logVerboseNS source = logWithoutLoc source levelVerbose

logMessage :: Q Exp
logMessage = logTH levelMessage

logMessageS :: Q Exp
logMessageS =
    [|\a b ->
    monadLoggerLog $(qLocation >>= liftLoc) a levelMessage (b :: Text)|]

logMessageN :: MonadLogger m => Text -> m ()
logMessageN = logWithoutLoc "" levelMessage

logMessageNS :: MonadLogger m => LogSource -> Text -> m ()
logMessageNS source = logWithoutLoc source levelMessage

logImportant :: Q Exp
logImportant = logTH levelImportant

logImportantS :: Q Exp
logImportantS =
    [|\a b ->
    monadLoggerLog $(qLocation >>= liftLoc) a levelImportant (b :: Text)|]

logImportantN :: MonadLogger m => Text -> m ()
logImportantN = logWithoutLoc "" levelImportant

logImportantNS :: MonadLogger m => LogSource -> Text -> m ()
logImportantNS source = logWithoutLoc source levelImportant

------------------------------------------------------------------------------
--- Replay Log
------------------------------------------------------------------------------

class CanReplayLog x where
    replayLog :: (MonadIO m, MonadLogger m) => LogSource -> x -> m ()

------------------------------------------------------------------------------
--- printException
------------------------------------------------------------------------------

printException :: (MonadIO m, MonadLogger m, Exception e) => e -> m ()
printException exc = do
    who <- liftIO $ whoCreated exc
    logErrorN $ "Replication error: " <> tshow exc <> "\n"
        <> fromString (intercalate "\n" who)

------------------------------------------------------------------------------
--- Display helpers
------------------------------------------------------------------------------

intercalateDisplay :: Display x => Text -> [x] -> Text
intercalateDisplay = intercalateDisplay' False

intercalateDisplay' :: Display x => Bool -> Text -> [x] -> Text
intercalateDisplay' prefix sep list
    | [] <- list = mempty
    | [ single ] <- list = bool "" sep prefix <> display single
    | (n : ns) <- list = bool "" sep prefix <> display n <> bool sep "" prefix
        <> intercalateDisplay' prefix sep ns
