module Baulig.Utils.DisplayBuilder
    ( DisplayBuilder
    , displayBuilder
    , displayBuilder'
    , (<+>)
    , (<+!>)
    , (<->)
    , (<-!>)
    , (<:>)
    , (<:!>)
    , (<:=>)
    , (<:=!>)
    , (<@>)
    , (<@!>)
    ) where

import           Baulig.Prelude.Import

import           Data.Traversable

import           Baulig.Utils.Logging

-----------------------------------------------------------------------------
-- DisplayBuilder
-----------------------------------------------------------------------------

data DisplayBuilder = DisplayBuilder { dbName :: !Text, dbElements :: ![Text] }

displayBuilder :: Typeable x => x -> DisplayBuilder
displayBuilder (showsTypeRep . typeOf -> name) =
    DisplayBuilder (fromString $ name "") []

displayBuilder' :: Text -> DisplayBuilder
displayBuilder' name = DisplayBuilder (display name) []

printBuilder :: DisplayBuilder -> Text
printBuilder builder =
    let inner = foldl' (\cur elm -> Just $ fromMaybe ":" cur <> " " <> elm)
                       Nothing $ reverse $ dbElements builder
    in "[" <> dbName builder <> fromMaybe mempty inner <> "]"

infixl 5 <+>

infixl 5 <+!>

infixl 5 <->

infixl 5 <-!>

infixl 5 <:>

infixl 5 <:!>

infixl 5 <:=>

infixl 5 <:=!>

infixl 5 <@>

infixl 5 <@!>

(<+>) :: Display x => DisplayBuilder -> x -> DisplayBuilder
(<+>) builder item = builder { dbElements = display item : dbElements builder }

(<+!>) :: Show x => DisplayBuilder -> x -> DisplayBuilder
(<+!>) builder item = builder { dbElements = tshow item : dbElements builder }

(<->) :: Display x => DisplayBuilder -> x -> DisplayBuilder
(<->) builder item =
    builder { dbElements = display item : "-" : dbElements builder }

(<-!>) :: Show x => DisplayBuilder -> x -> DisplayBuilder
(<-!>) builder item =
    builder { dbElements = tshow item : "-" : dbElements builder }

(<:>) :: (Display x, Typeable y) => y -> x -> DisplayBuilder
(<:>) name item = addElement (displayBuilder name) $ display item

(<:!>) :: (Show x, Typeable y) => y -> x -> DisplayBuilder
(<:!>) name item = addElement (displayBuilder name) $ tshow item

(<:=>) :: Display x => Text -> x -> DisplayBuilder
(<:=>) name item = addElement (displayBuilder' name) $ display item

(<:=!>) :: Show x => Text -> x -> DisplayBuilder
(<:=!>) name item = addElement (displayBuilder' name) $ tshow item

(<@>) :: (Traversable t, Display x) => DisplayBuilder -> t x -> DisplayBuilder
(<@>) builder items = addList builder $ items <&> display

(<@!>) :: (Traversable t, Show x) => DisplayBuilder -> t x -> DisplayBuilder
(<@!>) builder items = addList builder $ items <&> tshow

addElement :: DisplayBuilder -> Text -> DisplayBuilder
addElement builder item = builder { dbElements = item : dbElements builder }

addList :: Traversable t => DisplayBuilder -> t Text -> DisplayBuilder
addList builder items =
    let start = "[" : dbElements builder
        newItems = fst
            $ mapAccumL (\cur item -> (item : cur, absurd)) start items
    in builder { dbElements = "]" : newItems }

instance Display DisplayBuilder where
    display = printBuilder
