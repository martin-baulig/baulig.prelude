module Baulig.Utils.PrettyTable
    ( PrettyItem(..)
    , PrettyTable(..)
    , WhichName(..)
    ) where

import           Baulig.Prelude.Import

import qualified Data.List             as L

import           Text.Tabular
import           Text.Tabular.AsciiArt

------------------------------------------------------------------------------
--- PrettyPrintItem
------------------------------------------------------------------------------

class PrettyItem x where
    prettyItemName :: Proxy x -> Text

    prettyItemHeaders :: Proxy x -> [String]

    prettyItemToElements :: x -> [String]

    prettyPrintItem :: MonadIO m => x -> m ()
    prettyPrintItem = prettyPrintItemImpl

    prettyFormatItem :: x -> Text
    prettyFormatItem = prettyFormatItemImpl

------------------------------------------------------------------------------
--- PrettyPrint
------------------------------------------------------------------------------

data WhichName = NoItemName | TableName | ItemName
    deriving stock (Enum, Eq, Ord, Show)

class PrettyTable x where
    prettyTableName :: Proxy x -> WhichName -> Text

    prettyTableHeaders :: Proxy x -> [String]

    prettyTableItemToRow :: x -> [String]

    prettyPrintTable :: MonadIO m => Maybe Int -> [x] -> m ()
    prettyPrintTable = prettyPrintTableImpl

    prettyFormatTable :: Maybe Int -> [x] -> Text
    prettyFormatTable = prettyFormatTableImpl

    prettyPrintTableItem :: MonadIO m => Maybe Int -> x -> m ()
    prettyPrintTableItem = prettyPrintTableItemImpl

    prettyFormatTableItem :: Maybe Int -> x -> Text
    prettyFormatTableItem = prettyFormatTableItemImpl

------------------------------------------------------------------------------
--- Implementation Details
------------------------------------------------------------------------------

prettyPrintTableImpl
    :: forall x m. (PrettyTable x, MonadIO m) => Maybe Int -> [x] -> m ()
prettyPrintTableImpl selectedIndex items
    | null items = putStrLn [i|\nNo #{name NoItemName}.\n|]
    | otherwise = putStrLn $ "\n" <> name TableName <> ":\n"
        <> prettyFormatTable selectedIndex items
  where
    name = prettyTableName (Proxy :: Proxy x)

prettyFormatTableImpl :: forall x. PrettyTable x => Maybe Int -> [x] -> Text
prettyFormatTableImpl selectedIndex items = pack $ render id id id table
  where
    indices = Header <$> zipWith formatIndex [1 ..] items

    formatIndex :: Int -> x -> String
    formatIndex idx _
        | Just idx == selectedIndex = show idx ++ " (*)"
        | otherwise = show idx

    headers = Header <$> prettyTableHeaders (Proxy :: Proxy x)

    table = Table (Group NoLine indices) (Group DoubleLine headers)
        $ prettyTableItemToRow <$> items

prettyPrintTableItemImpl
    :: forall x m. (PrettyTable x, MonadIO m) => Maybe Int -> x -> m ()
prettyPrintTableItemImpl selectedIndex item = putStrLn $ "\n" <> name ItemName
    <> ":\n" <> prettyFormatTableItemImpl selectedIndex item
  where
    name = prettyTableName (Proxy :: Proxy x)

prettyFormatTableItemImpl :: forall x. PrettyTable x => Maybe Int -> x -> Text
prettyFormatTableItemImpl selectedIndex item = pack $ render id id id table
  where
    indices
        | Just idx <- selectedIndex = [Header $ show idx]
        | otherwise = [Header ""]

    headers = Header <$> prettyTableHeaders (Proxy :: Proxy x)

    table = Table (Group NoLine indices) (Group DoubleLine headers)
        $ prettyTableItemToRow <$> singleton item

prettyPrintItemImpl :: forall x m. (PrettyItem x, MonadIO m) => x -> m ()
prettyPrintItemImpl item =
    putStrLn $ "\n" <> name <> ":\n" <> prettyFormatItemImpl item
  where
    name = prettyItemName (Proxy :: Proxy x)

prettyFormatItemImpl :: forall x. PrettyItem x => x -> Text
prettyFormatItemImpl item = pack $ render id id id table
  where
    headers :: [Header String]
    headers = [Header ""]

    rowHeaders = Header
        <$> leftAlignTable (prettyItemHeaders (Proxy :: Proxy x))

    table = Table (Group NoLine rowHeaders) (Group NoLine headers) items

    items = map (: []) $ leftAlignTable $ prettyItemToElements item

padRight :: Int -> String -> String
padRight n s = s ++ replicate (n - length s) ' '

leftAlignTable :: [String] -> [String]
leftAlignTable rows = let maxWidth = L.maximum (L.map length rows)
                      in map (padRight maxWidth) rows

