module Baulig.Prelude.Lens
    ( L.view
    , module Lens.Micro
    , module Lens.Micro.Type
    ) where

import           ClassyPrelude

import qualified Control.Lens.Combinators as L

import           Lens.Micro               hiding (mapped)
import           Lens.Micro.Type
