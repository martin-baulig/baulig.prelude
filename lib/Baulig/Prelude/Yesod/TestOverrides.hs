module Baulig.Prelude.Yesod.TestOverrides (YesodTestOverrides(..)) where

import           Baulig.Prelude

import           Yesod.Core

------------------------------------------------------------------------------
--- YesodTestOverrides
------------------------------------------------------------------------------

class Yesod x => YesodTestOverrides x where
    type YesodTestOverrideType x

    yesodTestOverrides :: Lens' x (Maybe (YesodTestOverrideType x))
