{-# OPTIONS_GHC -Wno-orphans #-}

module Baulig.Prelude.Orphans () where

import           Baulig.Prelude.Import

import           Data.Time

import           Flat

import           Network.URI

import           Baulig.Utils.DisplayBuilder
import           Baulig.Utils.Logging

instance (Display x, Typeable x) => Display (Maybe x) where
    display maybeItem = case maybeItem of
        Just item -> display item
        Nothing -> let name = showsTypeRep $ typeOf maybeItem
                   in "Nothing(" <> fromString (name "") <> ")"

instance (Display x, Typeable x) => Display [x] where
    display items = display $ items <:> length items <@> items

instance Binary URIAuth

instance Binary URI

instance Display Bool where
    display = tshow

instance Display UTCTime where
    display = tshow

instance Display TimeZone where
    display = tshow

instance Display ZonedTime where
    display = tshow

instance Flat UTCTime where
    encode (UTCTime day dayTime) = encode (toModifiedJulianDay day)
        <> encode (toPicoInt64 dayTime)

    decode = do
        mjd <- ModifiedJulianDay <$> decode
        pico <- fromPicoInt64 <$> decode
        return $ UTCTime mjd pico

    size (UTCTime day dayTime) bitSize = size (toModifiedJulianDay day) bitSize
        + size (toPicoInt64 dayTime) bitSize

toPicoInt64 :: DiffTime -> Int64
toPicoInt64 = floor . (* 10 ^ (12 :: Int))

fromPicoInt64 :: Int64 -> DiffTime
fromPicoInt64 x = fromRational (toRational x / 10 ^ (12 :: Int))

